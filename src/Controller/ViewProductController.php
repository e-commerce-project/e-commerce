<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProductRepository;
use App\Entity\Product;

class ViewProductController extends Controller
{
    /**
     * @Route("/view/product/{id}", name="view_product")
     */
    public function index(int $id, ProductRepository $repo) {

        $repo = $this->getDoctrine()->getRepository(Product::class);

        $product = $repo->find($id);
                dump($product);
        return $this->render('view_product.html.twig', [
            
            "product" => $product
            
        ]);
    }
}