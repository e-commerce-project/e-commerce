<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\LineShoppingCart;
use App\Repository\ProductRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\ShoppingCart;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Product;

class LineShoppingCartController extends Controller
{
    /**
     * @Route("line_shopping_cart/user/{id}", name="line_shopping_cart")
     */
    public function index(Product $id, ObjectManager $manager)
    {
        $user = $this->getUser();//Je récupère le user
        $quantity = 1;
        $product = $id;
        if (!$user->getShoppingCart()) {//si le panier du user n'existe pas encore car vide
            $shoppingCart = new ShoppingCart();//crée une nouvelle instance de ShoppingCart
            $user->setShoppingCart($shoppingCart); //Je fixe au user son shoppingCart 
        }

        $shoppingCart = $user->getShoppingCart();//j'assigne à la variable shoppingCart le panier du user que j'ai récupéré
       // $product = $product->find($id);//Je récupère le product par son id

        $lineShoppingCart = null;
        foreach ($shoppingCart->getLineShoppingCarts() as $line) {
            if ($line->getProduct()->getId() == $product->getId()) {
                $lineShoppingCart = $line;
                $lineShoppingCart->setQuantity($line->getQuantity() + $quantity);
                $price = $product->getPrice() * $lineShoppingCart->getQuantity();
                $lineShoppingCart->setPrice($price);
                //$totalPrice = $price + $lineShoppingCarts;
            }
        }

        if (!$lineShoppingCart) {

            $lineShoppingCart = new LineShoppingCart(); //je crée une nouvelle instance de la classe/entité LineShoppingCart

            $lineShoppingCart->setProduct($product);

            $lineShoppingCart->setQuantity($quantity);

            $price = $product->getPrice() * $quantity;

            $lineShoppingCart->setPrice($price);

        }

        $shoppingCart->addLineShoppingCart($lineShoppingCart);

        $shoppingCart->setUser($user);
        dump($shoppingCart);
        $manager->persist($shoppingCart);
        $manager->flush();

        return $this->redirectToRoute("shopping_cart", []);
    }

    






        /*$lineShoppingCart->setProduct($product);
        $lineShoppingCart->setQuantity($quantity);//je fixe dans la variable lineShoppingCart la quantité de ce produit

        $price = $repository->getPrice() * $quantity;//je récupère le prix du produit dans la bdd que je multiplie par la quantité de ce produit pour avoir le prix total dans cette variable lineShoppingCart et je l'assigne à la variable price

        $lineShoppingCart->setPrice($price);// je fixe dans la variable lineShoppingCart le prix total du produit que j'ai récupéré dans la variable price
        
        $shoppingCart->setUser($user);//Je fixe le user concerné avec la méthode setUser dans le panier 
        $shoppingCart = addLineShoppingCart($lineShoppingCart);//Je rajoute cette nouvelle ligne au panier avec la méthode addLineShoppingCart en l'assignant à la variable shoppingCart
                      
        $manager->persist($shoppingCart);// je dis à Doctrine que je veux (éventuellement) sauvegarder ce panier (pas encore de requête)
        $manager->flush();// Je demande à Doctrine d'éxecuter réellement la requête

        return $this->return("line_shopping_cart.html.twig", []);*/

}
