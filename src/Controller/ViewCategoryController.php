<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Entity\Product;

class ViewCategoryController extends Controller
{
    //Ce controller permet de récupérer tous les produits d'une seule catégorie
    /**
     * @Route("/view/category/{category}", name="view_category")
     */
    public function index(Category $category, CategoryRepository $repo)
    {
        $repo = $this->getDoctrine()->getRepository(Product::class);
        $products = $category->getProducts();
        return $this->render('view_category/index.html.twig', [
            'category' => $category, 
            'products' => $products
        ]);
    }
}
